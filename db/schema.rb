# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140702200557) do

  create_table "sessions", force: true do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at"

  create_table "songs", force: true do |t|
    t.string   "filepath"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",     default: 0
  end

  create_table "songs_caches", force: true do |t|
    t.integer  "song_id"
    t.string   "title"
    t.string   "artist"
    t.string   "album"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "songs_caches", ["song_id"], name: "index_songs_caches_on_song_id"

  create_table "songs_tag_values", force: true do |t|
    t.integer "song_id"
    t.integer "tag_value_id"
  end

  add_index "songs_tag_values", ["song_id"], name: "index_songs_tag_values_on_song_id"
  add_index "songs_tag_values", ["tag_value_id"], name: "index_songs_tag_values_on_tag_value_id"

  create_table "tag_values", force: true do |t|
    t.integer  "tag_id"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tag_values", ["tag_id"], name: "index_tag_values_on_tag_id"

  create_table "tags", force: true do |t|
    t.string   "label"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
