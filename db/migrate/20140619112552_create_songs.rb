class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.string :filepath

      t.timestamps
    end
  end
end
