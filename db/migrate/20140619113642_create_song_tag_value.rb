class CreateSongTagValue < ActiveRecord::Migration
  def change
    create_table :songs_tag_values do |t|
      t.references :song, index: true
      t.references :tag_value, index: true
    end
  end
end
