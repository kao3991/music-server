class CreateTagValues < ActiveRecord::Migration
  def change
    create_table :tag_values do |t|
      t.references :tag, index: true
      t.string :value

      t.timestamps
    end
  end
end
