class CreateSongsCaches < ActiveRecord::Migration
  def change
    create_table :songs_caches do |t|
      t.references :song, index: true
      t.string :title
      t.string :artist
      t.string :album

      t.timestamps
    end
  end
end
