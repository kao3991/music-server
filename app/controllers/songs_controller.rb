class SongsController < ApplicationController
  def index
    @songs = SongsCache.all
  end

  def show
    @song = Song.find(params[:id])
    respond_to do |format|
      format.mp3 do
        send_file @song.filepath, :type => 'audio/mpeg'
      end
      format.js 
    end
  end
end
