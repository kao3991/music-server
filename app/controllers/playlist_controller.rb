class PlaylistController < ApplicationController
  before_action do
    @playlist = session[:playlist]
  end
  def show
    @playlist = session[:playlist]
    respond_to do |format|
      format.js 
    end
  end
  def add
    song = Song.find(params[:song_id])
    session[:playlist] = Array.new unless session[:playlist]
    if song
      session[:playlist] << song
    end
    render 'show'
  end
  def get_next
    @song = @playlist.shift
    redirect_to @song
  end
end
