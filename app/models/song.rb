class Song < ActiveRecord::Base
  has_and_belongs_to_many :tag_values
  has_many :tags, :through => :tag_values
  # default_scope do
  #   where(:status => 0)
  # end
  after_save :store_in_cache
  def tag(label)
    self.tag_values.where(:tag_id => Tag.get_by_label(label.to_s).id).first
  end
  def self.from_file(path)
    s = Song.find_or_create_by(:filepath => path)
    s.filepath = path
    begin
      ID3Tag.read(File.open(path)) do |tag|
        s.set_tag(:artist, tag.get_frame(:TPE1).content)
        s.set_tag(:title, tag.get_frame(:TIT2).content)
        s.set_tag(:album, tag.get_frame(:TALB).content)
        s.set_tag(:year, tag.get_frame(:TYER).content)
      end
    rescue
      raise
      s.status = 1
    end
    s.save!
  end

  def add_tag(label, tag_value)
    tag = Tag.get_by_label(label)
    tag_value = TagValue.get_by_value(tag, tag_value)
    self.tag_values << tag_value
  end

  def set_tag(label, tag_value)
    tag = Tag.get_by_label(label)
    if self.tag_values.where(tag: tag).exists?
      self.tag_values.where(tag: tag).destroy
    end
    tag_value = TagValue.get_by_value(tag, tag_value)
    self.tag_values << tag_value
  end
  def store_in_cache
    sc = SongsCache.find_or_create_by(song: self)
    sc.title = self.tag(:title).to_s
    sc.artist = self.tag(:artist).to_s
    sc.album = self.tag(:album).to_s
    sc.save
  end
  def self.import_collection
    files = Array.new
    startpath = Rails.application.config.music_path
    directories = Array.new
    directories << startpath
    directories.each do |path|
      begin
        path = path[-1] == '/' ? "#{path}*" : "#{path}/*"
        Dir.glob(path).each do |file|
          if File.directory?(file)
            directories << file unless directories.include?(file)
          else
            # Song.from_file(file)
            files << file
          end
        end
      rescue
        puts "reading #{path} failed"
        raise
      end
    end
    Song.all.each do |s|
      files << s.filepath
    end
    files.each do |file|
      Song.from_file file
    end
  end
end
