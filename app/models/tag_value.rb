class TagValue < ActiveRecord::Base
  belongs_to :tag
  has_and_belongs_to_many :songs

  def self.get_by_value(tag, value)
    tag.values.where(:value => value).first_or_create
  end

  def to_s
    self.value
  end
end
