class Tag < ActiveRecord::Base
  has_many :values, :class_name => 'TagValue'

  def self.get_by_label(value)
    Tag.where(:label => value).first_or_create
  end
end
